# API Project #

	This project consists on creating a REST application with queries and reports services.
	
	This project includes:
	- Java 8
	- Maven
	- Spring Boot
	- Derby

# How to build #

	mvn clean install

# How to run #

	mvn spring-boot:run
	
	*Initialization may take a little time because the data is inserted into Derby DB during startup.

# End points #

## Query Option will ask the user for the country name or code and print the airports & runways at each airport. ##
	/api/query/getAirportsAndRunwaysByCountry/
	
	Request Parameters:
	- countryCode
	- countryName
	
## 10 countries with highest number of airports (with count) and countries with lowest number of airports. ##
	/api/report/countriesWithHighestAndLowestNumberOfAirports/

## Type of runways per country ##
	/api/report/runwayTypeByCountry/
	
## Print the top 10 most common runway identifications ##
	/api/report/mostCommomRunwayIdentifications/