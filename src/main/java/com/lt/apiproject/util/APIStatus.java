package com.lt.apiproject.util;

/**
 * ENUM with status of api response
 * @author psilveira
 *
 */
public enum APIStatus {
	SUCCESS,
	ERROR
}
