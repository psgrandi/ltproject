package com.lt.apiproject.util;

/**
 * Class that is used to wrap api responses
 * @author psilveira
 *
 */
public class APIResponseMessage {

	final private APIStatus status;
	final private String code;
	final private String errorMessage;
	final private Object result;

	public APIResponseMessage(APIStatus status, String code, String errorMessage, Object result) {
		this.status = status;
		this.code = code;
		this.errorMessage = errorMessage;
		this.result = result;
	}

	public APIStatus getStatus() {
		return status;
	}

	public String getCode() {
		return code;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Object getResult() {
		return result;
	}

}
