package com.lt.apiproject.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Airport entity with an AirportBuilder
 * @author psilveira
 *
 */
@Entity
@Table
public class Airport implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String ident;
	private String type;
	private String name;
	private Double latitude;
	private Double longitude;
	private Double elevation;
	private String continent;
	private Country country;
	private String isoCountry;
	private String isoRegion;
	private String municipality;
	private String scheduledService;
	private String gpsCode;
	private String iataCode;
	private String localCode;
	private String homeLink;
	private List<Runway> runways;

	public Airport() {}
	
	public Airport(AirportBuilder builder) {
		this.id = builder.id;
		this.ident = builder.ident;
		this.type = builder.type;
		this.name = builder.name;
		this.latitude = builder.latitude;
		this.longitude = builder.longitude;
		this.elevation = builder.elevation;
		this.continent = builder.continent;
		this.country = builder.country;
		this.isoCountry = builder.isoCountry;
		this.isoRegion = builder.isoRegion;
		this.municipality = builder.municipality;
		this.scheduledService = builder.scheduledService;
		this.gpsCode = builder.gpsCode;
		this.iataCode = builder.iataCode;
		this.localCode = builder.localCode;
		this.homeLink = builder.homeLink;
		this.runways = builder.runways;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column
	public String getIdent() {
		return ident;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	@Column
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column
	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	@Column
	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	@ManyToOne
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	@JsonIgnore
	@Column
	public String getIsoCountry() {
		return isoCountry;
	}

	public void setIsoCountry(String isoCountry) {
		this.isoCountry = isoCountry;
	}

	@Column
	public String getIsoRegion() {
		return isoRegion;
	}

	public void setIsoRegion(String isoRegion) {
		this.isoRegion = isoRegion;
	}

	@Column
	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	@Column
	public String getScheduledService() {
		return scheduledService;
	}

	public void setScheduledService(String scheduledService) {
		this.scheduledService = scheduledService;
	}

	@Column
	public String getGpsCode() {
		return gpsCode;
	}

	public void setGpsCode(String gpsCode) {
		this.gpsCode = gpsCode;
	}

	@Column
	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	@Column
	public String getLocalCode() {
		return localCode;
	}

	public void setLocalCode(String localCode) {
		this.localCode = localCode;
	}

	@Column
	public String getHomeLink() {
		return homeLink;
	}

	public void setHomeLink(String homeLink) {
		this.homeLink = homeLink;
	}

	@OneToMany(mappedBy = "airport", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<Runway> getRunways() {
		return runways;
	}

	public void setRunways(List<Runway> runways) {
		this.runways = runways;
	}

	@Override
	public String toString() {
		return "Airport [id=" + id + ", ident=" + ident + ", type=" + type + ", name=" + name + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", elevation=" + elevation + ", continent=" + continent + ", country="
				+ country + ", isoCountry=" + isoCountry + ", isoRegion=" + isoRegion + ", municipality=" + municipality
				+ ", scheduledService=" + scheduledService + ", gpsCode=" + gpsCode + ", iataCode=" + iataCode
				+ ", localCode=" + localCode + ", homeLink=" + homeLink + "]";
	}

	public static class AirportBuilder {
		
		private Integer id;
		private String ident;
		private String type;
		private String name;
		private Double latitude;
		private Double longitude;
		private Double elevation;
		private String continent;
		private Country country;
		private String isoCountry;
		private String isoRegion;
		private String municipality;
		private String scheduledService;
		private String gpsCode;
		private String iataCode;
		private String localCode;
		private String homeLink;
		private List<Runway> runways;
		
		public AirportBuilder id(Integer id) {
			this.id = id;
			return this;
		}
		
		public AirportBuilder ident(String ident) {
			this.ident = ident;
			return this;
		}
		
		public AirportBuilder type(String type) {
			this.type = type;
			return this;
		}
		
		public AirportBuilder name(String name) {
			this.name = name;
			return this;
		}
		
		public AirportBuilder latitude(Double latitude) {
			this.latitude = latitude;
			return this;
		}
		
		public AirportBuilder longitude(Double longitude) {
			this.longitude = longitude;
			return this;
		}
		
		public AirportBuilder elevation(Double elevation) {
			this.elevation = elevation;
			return this;
		}
		
		public AirportBuilder continent(String continent) {
			this.continent = continent;
			return this;
		}
		
		public AirportBuilder country(Country country) {
			this.country = country;
			return this;
		}
		
		public AirportBuilder isoCountry(String isoCountry) {
			this.isoCountry = isoCountry;
			return this;
		}
		
		public AirportBuilder isoRegion(String isoRegion) {
			this.isoRegion = isoRegion;
			return this;
		}
		
		public AirportBuilder municipality(String municipality) {
			this.municipality = municipality;
			return this;
		}
		
		public AirportBuilder scheduledService(String scheduledService) {
			this.scheduledService = scheduledService;
			return this;
		}
		
		public AirportBuilder gpsCode(String gpsCode) {
			this.gpsCode = gpsCode;
			return this;
		}
		
		public AirportBuilder iataCode(String iataCode) {
			this.iataCode = iataCode;
			return this;
		}
		
		public AirportBuilder localCode(String localCode) {
			this.localCode = localCode;
			return this;
		}
		
		public AirportBuilder homeLink(String homeLink) {
			this.homeLink = homeLink;
			return this;
		}
		
		public AirportBuilder runways(List<Runway> runways) {
			this.runways = runways;
			return this;
		}
		
		public Airport build() {
			return new Airport(this);
		}
	}
	
}
