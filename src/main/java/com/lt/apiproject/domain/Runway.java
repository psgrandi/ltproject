package com.lt.apiproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Runway entity with a RunwayBuilder
 * @author psilveira
 *
 */
@Entity
@Table
public class Runway implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Airport airport;
	private Integer airportRef;
	private String airportIdent;
	private Double length;
	private Double width;
	private String surface;
	private boolean lighted;
	private boolean closed;
	private String leIdent;
	private Double leLatitude;
	private Double leLongitude;
	private Double leElevation;
	private String leHeadingDegT;
	private Double leDisplacedThreshold;
	private String heIdent;
	private Double heLatitude;
	private Double heLongitude;
	private Double heElevation;
	private String heHeadingDegT;
	private Double heDisplacedThreshold; 
	
	public Runway() {}
	
	public Runway(RunwayBuilder build) {
		this.id = build.id;
		this.airport = build.airport;
		this.airportRef = build.airportRef;
		this.airportIdent = build.airportIdent;
		this.length = build.length;
		this.width = build.width;
		this.surface = build.surface;
		this.lighted = build.lighted;
		this.closed = build.closed;
		this.leIdent = build.leIdent;
		this.leLatitude = build.leLatitude;
		this.leLongitude = build.leLongitude;
		this.leElevation = build.leElevation;
		this.leHeadingDegT = build.leHeadingDegT;
		this.leDisplacedThreshold = build.leDisplacedThreshold;
		this.heIdent = build.heIdent;
		this.heLatitude = build.heLatitude;
		this.heLongitude = build.heLongitude;
		this.heElevation = build.heElevation;
		this.heHeadingDegT = build.heHeadingDegT;
		this.heDisplacedThreshold = build.heDisplacedThreshold;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonIgnore
	@ManyToOne
	public Airport getAirport() {
		return airport;
	}
	public void setAirport(Airport airport) {
		this.airport = airport;
	}
	
	@Column
	public Integer getAirportRef() {
		return airportRef;
	}

	public void setAirportRef(Integer airportRef) {
		this.airportRef = airportRef;
	}

	@Column
	public String getAirportIdent() {
		return airportIdent;
	}

	public void setAirportIdent(String airportIdent) {
		this.airportIdent = airportIdent;
	}

	@Column
	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	@Column
	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	@Column
	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

	@Column
	public boolean isLighted() {
		return lighted;
	}

	public void setLighted(boolean lighted) {
		this.lighted = lighted;
	}

	@Column
	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	@Column
	public String getLeIdent() {
		return leIdent;
	}

	public void setLeIdent(String leIdent) {
		this.leIdent = leIdent;
	}

	@Column
	public Double getLeLatitude() {
		return leLatitude;
	}

	public void setLeLatitude(Double leLatitude) {
		this.leLatitude = leLatitude;
	}

	@Column
	public Double getLeLongitude() {
		return leLongitude;
	}

	public void setLeLongitude(Double leLongitude) {
		this.leLongitude = leLongitude;
	}

	@Column
	public Double getLeElevation() {
		return leElevation;
	}

	public void setLeElevation(Double leElevation) {
		this.leElevation = leElevation;
	}

	@Column
	public String getLeHeadingDegT() {
		return leHeadingDegT;
	}

	public void setLeHeadingDegT(String leHeadingDegT) {
		this.leHeadingDegT = leHeadingDegT;
	}

	@Column
	public Double getLeDisplacedThreshold() {
		return leDisplacedThreshold;
	}

	public void setLeDisplacedThreshold(Double leDisplacedThreshold) {
		this.leDisplacedThreshold = leDisplacedThreshold;
	}

	@Column
	public String getHeIdent() {
		return heIdent;
	}

	public void setHeIdent(String heIdent) {
		this.heIdent = heIdent;
	}

	@Column
	public Double getHeLatitude() {
		return heLatitude;
	}

	public void setHeLatitude(Double heLatitude) {
		this.heLatitude = heLatitude;
	}

	@Column
	public Double getHeLongitude() {
		return heLongitude;
	}

	public void setHeLongitude(Double heLongitude) {
		this.heLongitude = heLongitude;
	}

	@Column
	public Double getHeElevation() {
		return heElevation;
	}

	public void setHeElevation(Double heElevation) {
		this.heElevation = heElevation;
	}

	@Column
	public String getHeHeadingDegT() {
		return heHeadingDegT;
	}

	public void setHeHeadingDegT(String heHeadingDegT) {
		this.heHeadingDegT = heHeadingDegT;
	}

	@Column
	public Double getHeDisplacedThreshold() {
		return heDisplacedThreshold;
	}

	public void setHeDisplacedThreshold(Double heDisplacedThreshold) {
		this.heDisplacedThreshold = heDisplacedThreshold;
	}

	@Override
	public String toString() {
		return "Runway [id=" + id + ", airport=" + airport + ", airportRef=" + airportRef + ", airportIdent="
				+ airportIdent + ", length=" + length + ", width=" + width + ", surface=" + surface + ", lighted="
				+ lighted + ", closed=" + closed + ", leIdent=" + leIdent + ", leLatitude=" + leLatitude
				+ ", leLongitude=" + leLongitude + ", leElevation=" + leElevation + ", leHeadingDegT=" + leHeadingDegT
				+ ", leDisplacedThreshold=" + leDisplacedThreshold + ", heIdent=" + heIdent + ", heLatitude="
				+ heLatitude + ", heLongitude=" + heLongitude + ", heElevation=" + heElevation + ", heHeadingDegT="
				+ heHeadingDegT + ", heDisplacedThreshold=" + heDisplacedThreshold + "]";
	}

	public static class RunwayBuilder {
		
		private Integer id;
		private Airport airport;
		private Integer airportRef;
		private String airportIdent;
		private Double length;
		private Double width;
		private String surface;
		private boolean lighted;
		private boolean closed;
		private String leIdent;
		private Double leLatitude;
		private Double leLongitude;
		private Double leElevation;
		private String leHeadingDegT;
		private Double leDisplacedThreshold;
		private String heIdent;
		private Double heLatitude;
		private Double heLongitude;
		private Double heElevation;
		private String heHeadingDegT;
		private Double heDisplacedThreshold; 
		
		public RunwayBuilder id(Integer id) {
			this.id = id;
			return this;
		}
		
		public RunwayBuilder airport(Airport airport) {
			this.airport = airport;
			return this;
		}
		
		public RunwayBuilder airportRef(Integer airportRef) {
			this.airportRef = airportRef;
			return this;
		}
		
		public RunwayBuilder airportIdent(String airportIdent) {
			this.airportIdent = airportIdent;
			return this;
		}
		
		public RunwayBuilder length(Double length) {
			this.length = length;
			return this;
		}
		
		public RunwayBuilder width(Double width) {
			this.width = width;
			return this;
		}
		
		public RunwayBuilder surface(String surface) {
			this.surface = surface;
			return this;
		}
		
		public RunwayBuilder lighted(boolean lighted) {
			this.lighted = lighted;
			return this;
		}
		
		public RunwayBuilder closed(boolean closed) {
			this.closed = closed;
			return this;
		}
		
		public RunwayBuilder leIdent(String leIdent) {
			this.leIdent = leIdent;
			return this;
		}
		
		public RunwayBuilder leLatitude(Double leLatitude) {
			this.leLatitude = leLatitude;
			return this;
		}
		
		public RunwayBuilder leLongitude(Double leLongitude) {
			this.leLongitude = leLongitude;
			return this;
		}
		
		public RunwayBuilder leElevation(Double leElevation) {
			this.leElevation = leElevation;
			return this;
		}
		
		public RunwayBuilder leHeadingDegT(String leHeadingDegT) {
			this.leHeadingDegT = leHeadingDegT;
			return this;
		}
		
		public RunwayBuilder leDisplacedThreshold(Double leDisplacedThreshold) {
			this.leDisplacedThreshold = leDisplacedThreshold;
			return this;
		}
		
		public RunwayBuilder heIdent(String heIdent) {
			this.heIdent = heIdent;
			return this;
		}
		
		public RunwayBuilder heLatitude(Double heLatitude) {
			this.heLatitude = heLatitude;
			return this;
		}
		
		public RunwayBuilder heLongitude(Double heLongitude) {
			this.heLongitude = heLongitude;
			return this;
		}
		
		public RunwayBuilder heElevation(Double heElevation) {
			this.heElevation = heElevation;
			return this;
		}
		
		public RunwayBuilder heHeadingDegT(String heHeadingDegT) {
			this.heHeadingDegT = heHeadingDegT;
			return this;
		}
		
		public RunwayBuilder heDisplacedThreshold(Double heDisplacedThreshold) {
			this.heDisplacedThreshold = heDisplacedThreshold;
			return this;
		}
		
		public Runway build() {
			return new Runway(this);
		}
	}
	
}
