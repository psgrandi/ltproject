package com.lt.apiproject.domain.report;

import com.lt.apiproject.domain.Country;

/**
 * Class that wrap the result from report of number of airports by country
 * @author psilveira
 *
 */
public class NumberOfAirportsByCountry {

	private Country country;
	private Long total;

	public NumberOfAirportsByCountry(Country country, Long total) {
		this.country = country;
		this.total = total;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
