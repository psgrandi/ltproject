package com.lt.apiproject.domain.report;

/**
 * Class that wrap the result from report of most commom runway identifications
 * @author psilveira
 *
 */
public class MostCommomRunwayIdentifications {

	private String runwayIdentification;
	private Long total;

	public MostCommomRunwayIdentifications(String runwayIdentification, Long total) {
		this.runwayIdentification = runwayIdentification;
		this.total = total;
	}

	public String getRunwayIdentification() {
		return runwayIdentification;
	}

	public void setRunwayIdentification(String runwayIdentification) {
		this.runwayIdentification = runwayIdentification;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
