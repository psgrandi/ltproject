package com.lt.apiproject.domain.report;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lt.apiproject.domain.Country;

/**
 * Class that wrap the result from report of runway types by country
 * @author psilveira
 *
 */
public class RunwayTypeByCountry {

	private Country country;
	private String runwayType;
	private Set<String> runwayTypeList;

	public RunwayTypeByCountry() {}
	
	public RunwayTypeByCountry(Country country, String runwayType) {
		this.country = country;
		this.runwayType = runwayType;
	}
	
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@JsonIgnore
	public String getRunwayType() {
		return runwayType;
	}

	public void setRunwayType(String runwayType) {
		this.runwayType = runwayType;
	}

	public Set<String> getRunwayTypeList() {
		return runwayTypeList;
	}

	public void setRunwayTypeList(Set<String> runwayTypeList) {
		this.runwayTypeList = runwayTypeList;
	}
	
	public void addRunwayType(String newRunwayType) {
		if (this.runwayTypeList == null) {
			this.runwayTypeList = new HashSet<>();
		}
		
		this.runwayTypeList.add(newRunwayType);
	}

}
