package com.lt.apiproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Country entity with a CountryBuilder
 * @author psilveira
 *
 */
@Entity
@Table
public class Country implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String code;
	private String name;
	private String continent;
	private String wikipediaLink;

	public Country() {}
	
	public Country(CountryBuilder builder) {
		this.id = builder.id;
		this.code = builder.code;
		this.name = builder.name;
		this.continent = builder.continent;
		this.wikipediaLink = builder.wikipediaLink;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column
	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	@Column
	public String getWikipediaLink() {
		return wikipediaLink;
	}

	public void setWikipediaLink(String wikipediaLink) {
		this.wikipediaLink = wikipediaLink;
	}
	
	public static class CountryBuilder {
		
		private Integer id;
		private String code;
		private String name;
		private String continent;
		private String wikipediaLink;
		
		public CountryBuilder() {}

		public CountryBuilder id(Integer id) {
			this.id = id;
			return this;
		}
		
		public CountryBuilder code(String code) {
			this.code = code;
			return this;
		}
		
		public CountryBuilder name(String name) {
			this.name = name;
			return this;
		}
		
		public CountryBuilder continent(String continent) {
			this.continent = continent;
			return this;
		}
		
		public CountryBuilder wikipediaLink(String wikipediaLink) {
			this.wikipediaLink = wikipediaLink;
			return this;
		}
		
		public Country build() {
			return new Country(this);
		}
	}
}
