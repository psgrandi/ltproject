package com.lt.apiproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lt.apiproject.domain.Runway;
import com.lt.apiproject.domain.report.MostCommomRunwayIdentifications;
import com.lt.apiproject.domain.report.RunwayTypeByCountry;

/**
 * DAO interface that performs queries for runway domain
 * @author psilveira
 *
 */
@Repository
public interface RunwayRepository extends JpaRepository<Runway, Integer>{
	
	@Query("select distinct new com.lt.apiproject.domain.report.RunwayTypeByCountry(c, r.surface) "
			+ "from Runway r, Airport a, Country c "
			+ "where r.airport.id = a.id "
			+ "and a.country.id = c.id")
	List<RunwayTypeByCountry> getRunwayTypeByCountry();
	
	@Query("select new com.lt.apiproject.domain.report.MostCommomRunwayIdentifications(r.leIdent, count(1)) "
			+ "from Runway r "
			+ "group by r.leIdent "
			+ "order by count(1) ")
	List<MostCommomRunwayIdentifications> getMostCommomRunwayIdentifications();

}
