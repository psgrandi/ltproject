package com.lt.apiproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lt.apiproject.domain.Country;

/**
 * DAO interface that performs queries for country domain
 * @author psilveira
 *
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Integer>{

	List<Country> findByNameIgnoreCaseContaining(String name);
	
	List<Country> findByCode(String code);
}
