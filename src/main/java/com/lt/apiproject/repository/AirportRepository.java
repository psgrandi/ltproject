package com.lt.apiproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.Country;
import com.lt.apiproject.domain.report.NumberOfAirportsByCountry;

/**
 * DAO interface that performs queries for airport domain
 * @author psilveira
 *
 */
@Repository
public interface AirportRepository extends JpaRepository<Airport, Integer>{
	
	List<Airport> findByCountry(Country country);

	@Query("select new com.lt.apiproject.domain.report.NumberOfAirportsByCountry(c, count(a)) "
			+ "from Airport a, Country c "
			+ "where a.country.id = c.id "
			+ "group by c")
	List<NumberOfAirportsByCountry> getNumberOfAirportsByCountry();
	
}
