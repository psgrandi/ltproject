package com.lt.apiproject.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.report.MostCommomRunwayIdentifications;
import com.lt.apiproject.domain.report.NumberOfAirportsByCountry;
import com.lt.apiproject.domain.report.RunwayTypeByCountry;
import com.lt.apiproject.service.APIService;
import com.lt.apiproject.util.APIResponseMessage;
import com.lt.apiproject.util.APIStatus;

/**
 * Rest Controller that receives requests for query and reports methods regarding Countries, Airports and Runways.
 * @author psilveira
 *
 */
@RestController
@RequestMapping("/api")
public class APIController {
	
	@Autowired
	private APIService apiService;

	@RequestMapping(value = "test", method = RequestMethod.GET)
	public ResponseEntity<?> test() {
		return new ResponseEntity<APIResponseMessage>(
				new APIResponseMessage(APIStatus.SUCCESS
						, HttpStatus.OK.toString()
						, null
						, "API Project is alive!")
				, HttpStatus.OK);
	}
	
	/**
	 * Endpoint that receives a countryCode or countryName by request parameter and returns a list of Airports and Runways.
	 *  
	 * @param countryCode
	 * @param countryName
	 * @return APIResponseMessage
	 */
	@RequestMapping(value = "/query/getAirportsAndRunwaysByCountry", method = RequestMethod.GET)
	public ResponseEntity<APIResponseMessage> getAiportsAndRunwaysByCountry(@RequestParam(value = "countryCode", required = false) String countryCode,
			@RequestParam(value = "countryName", required = false) String countryName) {
		
		if ((countryName == null || countryName == "") && (countryCode == null || countryCode == "")) {
			return new ResponseEntity<APIResponseMessage>(
					new APIResponseMessage(APIStatus.ERROR
							, HttpStatus.BAD_REQUEST.toString()
							, "To access this service. it's required to use request parameters countryName or countryCode"
							, null)
					, HttpStatus.BAD_REQUEST);
		}
		
		List<Airport> result = apiService.getAirportAndRunwaysByCountry(countryCode, countryName);
				
		return new ResponseEntity<APIResponseMessage>(
				new APIResponseMessage(APIStatus.SUCCESS
						, HttpStatus.OK.toString()
						, null
						, result)
				, HttpStatus.OK);	
	}
	
	/**
	 * Endpoint that generate report with countries with highest (top 10) and lowest (bottom 10) number of airports.
	 * 
	 * @return APIResponseMessage
	 */
	@RequestMapping(value = "/report/countriesWithHighestAndLowestNumberOfAirports", method = RequestMethod.GET)
	public ResponseEntity<APIResponseMessage> getCountriesWithHighestAndLowestNumberOfAirports() {
		HashMap<String,List<NumberOfAirportsByCountry>>  result = apiService.getCountriesWithHighestAndLowestNumberOfAirports();
		
		return new ResponseEntity<APIResponseMessage>(
				new APIResponseMessage(APIStatus.SUCCESS
						, HttpStatus.OK.toString()
						, null
						, result)
				, HttpStatus.OK);
	}
	
	/**
	 * Endpoint that generate report with types of runways by country.
	 * 
	 * @return APIResponseMessage
	 */
	@RequestMapping(value = "/report/runwayTypeByCountry", method = RequestMethod.GET)
	public ResponseEntity<APIResponseMessage> getRunwayTypeByCountry() {
		List<RunwayTypeByCountry> result = apiService.getRunwayTypeByCountry();
		
		return new ResponseEntity<APIResponseMessage>(
				new APIResponseMessage(APIStatus.SUCCESS
						, HttpStatus.OK.toString()
						, null
						, result)
				, HttpStatus.OK);
	}
	
	/**
	 * Endpoint that generate report with most commom runways identifications
	 * 
	 * @return APIResponseMessage
	 */
	@RequestMapping(value = "/report/mostCommomRunwayIdentifications", method = RequestMethod.GET)
	public ResponseEntity<APIResponseMessage> getMostCommomRunwayIdentifications() {
		List<MostCommomRunwayIdentifications> result = apiService.getMostCommomRunwayIdentifications();
		
		return new ResponseEntity<APIResponseMessage>(
				new APIResponseMessage(APIStatus.SUCCESS
						, HttpStatus.OK.toString()
						, null
						, result)
				, HttpStatus.OK);
	}
}
