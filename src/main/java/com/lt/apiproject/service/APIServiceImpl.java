package com.lt.apiproject.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.Country;
import com.lt.apiproject.domain.report.MostCommomRunwayIdentifications;
import com.lt.apiproject.domain.report.NumberOfAirportsByCountry;
import com.lt.apiproject.domain.report.RunwayTypeByCountry;
import com.lt.apiproject.repository.AirportRepository;
import com.lt.apiproject.repository.CountryRepository;
import com.lt.apiproject.repository.RunwayRepository;

/**
 * Implementation of APIService interface
 * @author psilveira
 *
 */
@Service
public class APIServiceImpl implements APIService {
	
	@Autowired
	private CountryRepository countryDAO;

	@Autowired
	private AirportRepository airportDAO;
	
	@Autowired
	private RunwayRepository runwayDAO;
		
	public List<Airport> getAirportAndRunwaysByCountry(String countryCode, String countryName) {
		List<Country> countries = new ArrayList<>();
		
		if (countryCode != null) {
			countries = countryDAO.findByCode(countryCode);
		} else {
			countries = countryDAO.findByNameIgnoreCaseContaining("%"+countryName+"%");
		}
		
		return getAirportsByCountry(countries);
	}
		
	public List<Airport> getAirportsByCountry(List<Country> countries) {
		List<Airport> airports = new ArrayList<>();
		
		countries.stream()
				.forEach(country -> airports.addAll(airportDAO.findByCountry(country)));
		
		return airports;
	}
	
	public HashMap<String,List<NumberOfAirportsByCountry>> getCountriesWithHighestAndLowestNumberOfAirports() {
		
		List<NumberOfAirportsByCountry> airportsByCountry = airportDAO.getNumberOfAirportsByCountry();
		
		HashMap<String,List<NumberOfAirportsByCountry>> result = new HashMap<>();
		
		result.put("highestNumberOfAirports", airportsByCountry.stream()
										.sorted(Comparator.comparing(NumberOfAirportsByCountry::getTotal).reversed())
										.limit(10)
										.collect(Collectors.toList()));
		
		result.put("lowestNumberOfAirports", airportsByCountry.stream()
										.sorted(Comparator.comparing(NumberOfAirportsByCountry::getTotal))
										.limit(10)
										.collect(Collectors.toList()));
		
		return result;
	}
	
	
	public List<RunwayTypeByCountry> getRunwayTypeByCountry() {
		HashMap<Country, RunwayTypeByCountry> result = new HashMap<>();
		
		List<RunwayTypeByCountry> runwayTypes = runwayDAO.getRunwayTypeByCountry();
		
		for (RunwayTypeByCountry item : runwayTypes) {
			
			Country country = item.getCountry();
			String runwayType = item.getRunwayType();
			
			if (!result.containsKey(country)) {
				RunwayTypeByCountry runwayTypeReport = new RunwayTypeByCountry();
				runwayTypeReport.setCountry(country);
				runwayTypeReport.addRunwayType(runwayType);
				
				result.put(country, runwayTypeReport);
			} else {
				result.get(country).addRunwayType(runwayType);
			}
			
		}
		
		return result.values().stream().collect(Collectors.toList());
	}
	
	public List<MostCommomRunwayIdentifications> getMostCommomRunwayIdentifications() {
		
		return runwayDAO.getMostCommomRunwayIdentifications()
					.stream()
					.sorted(Comparator.comparing(MostCommomRunwayIdentifications::getTotal).reversed())
					.limit(10)
					.collect(Collectors.toList());
	}
	
}