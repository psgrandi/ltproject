package com.lt.apiproject.service;

import java.util.HashMap;
import java.util.List;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.report.MostCommomRunwayIdentifications;
import com.lt.apiproject.domain.report.NumberOfAirportsByCountry;
import com.lt.apiproject.domain.report.RunwayTypeByCountry;

/**
 * Service layer that format data received by repository layer
 * @author psilveira
 *
 */
public interface APIService {
	
	/**
	 * Receives a countryCode and a countryName and get all airports and runways from this/these country(ies)
	 * 
	 * @param countryCode
	 * @param countryName
	 * @return List<Airport>
	 */
	List<Airport> getAirportAndRunwaysByCountry(String countryCode, String countryName);
	
	/**
	 * Get all airports by country and generates report with highest and lowest number of airports
	 * @return
	 */
	HashMap<String,List<NumberOfAirportsByCountry>> getCountriesWithHighestAndLowestNumberOfAirports();
	
	/**
	 * Get all countries and all distinct runway types.
	 * @return
	 */
	List<RunwayTypeByCountry> getRunwayTypeByCountry();
	
	/**
	 * Get all runways and generate report with most commom identifications 
	 * @return
	 */
	List<MostCommomRunwayIdentifications> getMostCommomRunwayIdentifications();
	
}
