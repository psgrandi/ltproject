package com.lt.apiproject.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.lt.apiproject.ApplicationTest;
import com.lt.apiproject.builder.AirportBuilder;
import com.lt.apiproject.builder.CountryBuilder;
import com.lt.apiproject.builder.RunwayBuilder;
import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.Country;
import com.lt.apiproject.domain.Runway;
import com.lt.apiproject.domain.report.MostCommomRunwayIdentifications;
import com.lt.apiproject.domain.report.NumberOfAirportsByCountry;
import com.lt.apiproject.domain.report.RunwayTypeByCountry;
import com.lt.apiproject.service.APIService;
import com.lt.apiproject.util.APIStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ApplicationTest.class)
@AutoConfigureMockMvc
public class APIControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private APIService apiService;
	
	private Country country;
	private Airport airport;
	private Runway runway;
	
	@Before
	public void init() {
		this.country = new CountryBuilder().getCountry();
		this.airport = new AirportBuilder().getAirport(this.country);
		this.runway = new RunwayBuilder().getRunway(this.airport);
		
		List<Runway> listOfRunways = new ArrayList<>();
		listOfRunways.add(this.runway);
		
		this.airport.setRunways(listOfRunways);
	}
		
	@Test
	public void mustReturnAirportsAndRunwaysByCountry() throws Exception {
		
		List<Airport> listOfAirports = new ArrayList<>();
		listOfAirports.add(this.airport);
		
		Mockito.when(
				apiService.getAirportAndRunwaysByCountry("US", null)).thenReturn(listOfAirports);
		
		mockMvc.perform(get("/api/query/getAirportsAndRunwaysByCountry/")
			      .contentType(MediaType.APPLICATION_JSON)
			      .param("countryCode", "US"))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status", is(APIStatus.SUCCESS.toString())))
			      .andExpect(jsonPath("$.code", is("200")))
			      .andExpect(jsonPath("$.errorMessage", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].id", is(6523)))
			      .andExpect(jsonPath("$.result.[0].ident", is("00A")))
			      .andExpect(jsonPath("$.result.[0].type", is("heliport")))
			      .andExpect(jsonPath("$.result.[0].name", is("Total Rf Heliport")))
			      .andExpect(jsonPath("$.result.[0].latitude", is(40.07080078125)))
			      .andExpect(jsonPath("$.result.[0].longitude", is(-74.93360137939453)))
			      .andExpect(jsonPath("$.result.[0].elevation", is(11.0)))
			      .andExpect(jsonPath("$.result.[0].continent", is("NA")))
			      .andExpect(jsonPath("$.result.[0].country.id", is(302755)))
			      .andExpect(jsonPath("$.result.[0].country.code", is("US")))
			      .andExpect(jsonPath("$.result.[0].country.name", is("United States")))
			      .andExpect(jsonPath("$.result.[0].country.continent", is("NA")))
			      .andExpect(jsonPath("$.result.[0].country.wikipediaLink", is("http://en.wikipedia.org/wiki/United_States")))
			      .andExpect(jsonPath("$.result.[0].isoRegion", is("US-PA")))
			      .andExpect(jsonPath("$.result.[0].municipality", is("Bensalem")))
			      .andExpect(jsonPath("$.result.[0].scheduledService", is("no")))
			      .andExpect(jsonPath("$.result.[0].gpsCode", is("00A")))
			      .andExpect(jsonPath("$.result.[0].iataCode", is("")))
			      .andExpect(jsonPath("$.result.[0].localCode", is("00A")))
			      .andExpect(jsonPath("$.result.[0].homeLink", is("")))
			      .andExpect(jsonPath("$.result.[0].runways.[0].id", is(269408)))
			      .andExpect(jsonPath("$.result.[0].runways.[0].airportRef", is(6523)))
			      .andExpect(jsonPath("$.result.[0].runways.[0].airportIdent", is("00A")))
			      .andExpect(jsonPath("$.result.[0].runways.[0].length", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].width", is(80.0)))
			      .andExpect(jsonPath("$.result.[0].runways.[0].surface", is("ASPH-G")))
			      .andExpect(jsonPath("$.result.[0].runways.[0].lighted", is(true)))
			      .andExpect(jsonPath("$.result.[0].runways.[0].closed", is(false)))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leIdent", is("H1")))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leLatitude", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leLongitude", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leElevation", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leHeadingDegT", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].leDisplacedThreshold", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].heLatitude", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].heLongitude", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].heElevation", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].heHeadingDegT", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runways.[0].heDisplacedThreshold", is(IsNull.nullValue())))
			      .andDo(print());
	}	
	
	@Test
	public void mustReturnAPIExceptionForMissingRequestParameter() throws Exception {
		
		mockMvc.perform(get("/api/query/getAirportsAndRunwaysByCountry/")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isBadRequest())
			      .andExpect(jsonPath("$.status", is(APIStatus.ERROR.toString())))
			      .andExpect(jsonPath("$.code", is("400")))
			      .andExpect(jsonPath("$.errorMessage", is("To access this service. it's required to use request parameters countryName or countryCode")));
	}
	
	@Test
	public void mustReturnCountriesWithHighestAndLowestNumberOfAirports() throws Exception {
		
		NumberOfAirportsByCountry airportsByCountry = new NumberOfAirportsByCountry(this.country, new Long(1000));
				
		List<NumberOfAirportsByCountry> listOfAirportsByCountry = new ArrayList<>();
		listOfAirportsByCountry.add(airportsByCountry);
		
		HashMap<String,List<NumberOfAirportsByCountry>> report = new HashMap<>();
		report.put("highestNumberOfAirports", listOfAirportsByCountry);
		report.put("lowestNumberOfAirports", listOfAirportsByCountry);
		
		Mockito.when(
				apiService.getCountriesWithHighestAndLowestNumberOfAirports()).thenReturn(report);
				
		mockMvc.perform(get("/api/report/countriesWithHighestAndLowestNumberOfAirports/")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status", is(APIStatus.SUCCESS.toString())))
			      .andExpect(jsonPath("$.code", is("200")))
			      .andExpect(jsonPath("$.errorMessage", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].country.id", is(302755)))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].country.code", is("US")))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].country.name", is("United States")))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].country.continent", is("NA")))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].country.wikipediaLink", is("http://en.wikipedia.org/wiki/United_States")))
			      .andExpect(jsonPath("$.result.highestNumberOfAirports.[0].total", is(1000)))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].country.id", is(302755)))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].country.code", is("US")))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].country.name", is("United States")))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].country.continent", is("NA")))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].country.wikipediaLink", is("http://en.wikipedia.org/wiki/United_States")))
			      .andExpect(jsonPath("$.result.lowestNumberOfAirports.[0].total", is(1000)));			
	}
	
	@Test
	public void mustReturnRunwayTypeByCountry() throws Exception {
		
		RunwayTypeByCountry runwayTypeByCountry = new RunwayTypeByCountry();
		runwayTypeByCountry.setCountry(this.country);
		runwayTypeByCountry.addRunwayType(this.runway.getSurface());
		
		List<RunwayTypeByCountry> report = new ArrayList<>();
		report.add(runwayTypeByCountry);
		
		Mockito.when(
				apiService.getRunwayTypeByCountry()).thenReturn(report);
		
		mockMvc.perform(get("/api/report/runwayTypeByCountry/")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status", is(APIStatus.SUCCESS.toString())))
			      .andExpect(jsonPath("$.code", is("200")))
			      .andExpect(jsonPath("$.errorMessage", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].country.id", is(302755)))
			      .andExpect(jsonPath("$.result.[0].country.code", is("US")))
			      .andExpect(jsonPath("$.result.[0].country.name", is("United States")))
			      .andExpect(jsonPath("$.result.[0].country.continent", is("NA")))
			      .andExpect(jsonPath("$.result.[0].country.wikipediaLink", is("http://en.wikipedia.org/wiki/United_States")))
			      .andExpect(jsonPath("$.result.[0].runwayTypeList.[0]", is("ASPH-G")));	
	}
	
	@Test
	public void mustReturnMostCommomRunwayIdentifications() throws Exception {
		
		MostCommomRunwayIdentifications runwayIdentification = new MostCommomRunwayIdentifications(this.runway.getLeIdent(), new Long(1000));
		
		List<MostCommomRunwayIdentifications> report = apiService.getMostCommomRunwayIdentifications();
		report.add(runwayIdentification);
		
		Mockito.when(
				apiService.getMostCommomRunwayIdentifications()).thenReturn(report);
		
		mockMvc.perform(get("/api/report/mostCommomRunwayIdentifications/")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.status", is(APIStatus.SUCCESS.toString())))
			      .andExpect(jsonPath("$.code", is("200")))
			      .andExpect(jsonPath("$.errorMessage", is(IsNull.nullValue())))
			      .andExpect(jsonPath("$.result.[0].runwayIdentification", is(this.runway.getLeIdent())))
			      .andExpect(jsonPath("$.result.[0].total", is(1000)));	
	}
}
