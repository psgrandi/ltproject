package com.lt.apiproject.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.lt.apiproject.builder.CountryBuilder;
import com.lt.apiproject.domain.Country;
import com.lt.apiproject.repository.CountryRepository;
import com.lt.apiproject.service.APIService;
import com.lt.apiproject.service.APIServiceImpl;

@RunWith(SpringRunner.class)
public class APIServiceImplTest {
	
	@TestConfiguration
    static class APIServiceImplTestContextConfiguration {
  
        @Bean
        public APIService apiService() {
            return new APIServiceImpl();
        }
    }

	@MockBean
	private CountryRepository countryDAO;
	 	
	@Autowired
	private APIService apiService;
	
	private Country country;
	
	@Before
	public void init() {
		this.country = new CountryBuilder().getCountry();
				
		List<Country> listOfCountries = new ArrayList<>();
		listOfCountries.add(this.country);
		
		Mockito.when(countryDAO.findByCode("US")).thenReturn(listOfCountries);
	}
	
	@Test
	public void mustCallFindByCode() {
		
		String countryCode = "BR";
		
		apiService.getAirportAndRunwaysByCountry(countryCode, null);
		
		verify(countryDAO, times(1)).findByCode(countryCode);		
		
	}
	
	@Test
	public void mustCallFindByNameIgnoreCaseContaining() {
		
		String countryName = "Brazil";
		
		apiService.getAirportAndRunwaysByCountry(null, countryName);
		
		verify(countryDAO, times(1)).findByNameIgnoreCaseContaining("%"+countryName+"%");		
		
	}
	
}
