package com.lt.apiproject.builder;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.Country;

public class AirportBuilder {
	
//	"id": 6523,
//    "ident": "00A",
//    "type": "heliport",
//    "name": "Total Rf Heliport",
//    "latitude": 40.07080078125,
//    "longitude": -74.93360137939453,
//    "elevation": 11,
//    "continent": "NA",
//    "country": {
//        "id": 302755,
//        "code": "US",
//        "name": "United States",
//        "continent": "NA",
//        "wikipediaLink": "http://en.wikipedia.org/wiki/United_States"
//    },
//    "isoRegion": "US-PA",
//    "municipality": "Bensalem",
//    "scheduledService": "no",
//    "gpsCode": "00A",
//    "iataCode": "",
//    "localCode": "00A",
//    "homeLink": "",

	public Airport getAirport(Country country) {
		return new Airport.AirportBuilder()
				.id(6523)
				.ident("00A")
				.type("heliport")
				.name("Total Rf Heliport")
				.latitude(40.07080078125)
				.longitude(-74.93360137939453)
				.elevation(11.0)
				.continent("NA")
				.country(country)
				.isoRegion("US-PA")
				.municipality("Bensalem")
				.scheduledService("no")
				.gpsCode("00A")
				.iataCode("")
				.localCode("00A")
				.homeLink("")
				.build();					
	}
	
}
