package com.lt.apiproject.builder;

import com.lt.apiproject.domain.Country;

public class CountryBuilder {
	
	public Country getCountry() {
		return new Country.CountryBuilder()
				.id(302755)
				.code("US")
				.name("United States")
				.continent("NA")
				.wikipediaLink("http://en.wikipedia.org/wiki/United_States")
				.build();					
	}
	
}
