package com.lt.apiproject.builder;

import com.lt.apiproject.domain.Airport;
import com.lt.apiproject.domain.Runway;

public class RunwayBuilder {
	
	public Runway getRunway(Airport airport) {
		return new Runway.RunwayBuilder()
				.id(269408)
				.airport(airport)
				.airportRef(6523)
				.airportIdent("00A")
				.length(null)
				.width(80.0)
				.surface("ASPH-G")
				.lighted(true)
				.closed(false)
				.leIdent("H1")
				.leLatitude(null)
				.leLongitude(null)
				.leElevation(null)
				.leHeadingDegT(null)
				.leDisplacedThreshold(null)
				.heIdent(null)
				.heLatitude(null)
				.heLongitude(null)
				.heElevation(null)
				.heHeadingDegT(null)
				.heDisplacedThreshold(null)
				.build();
	}
				
}
